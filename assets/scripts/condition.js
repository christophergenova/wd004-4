//conditions -- these are statements that will either result to true or false;

//if
//if(condition){
	//task you want to execute.
//}


//if else statement
function checkAge(age){
	if(age>=18) {
		return "legal ka na"
	}else{
		return "Minor ka pa"
	};

}
//
// if-else
// if(condition){
	//task you want to execute if the condition is true 
	//} else {
		//task you want to execute if the condition is false;

	//}

// /**This function checks the age and returns a string value.
// @param {number} var is age User-input age
// */
	function whereYouShouldBe(age) {
		//0-5 with mommy/daddy
		//6-12 Elementary school
		//13-18 High school
		//19-22 College
		//22-23 Soul searching
		//24-50 Work
		//51- Enjoying life
		if(age <= 5) {
			return "With mommy/daddy";

		}else if(age<=12){
			return "Elementary School";
		}else if(age<=18){
			return "High school";
		}else if(age <=22){
			return "college";

		}else if(age <=23){
			return "soul searching";
		}else if(age <=50){
			return "work";
		}else{
			return "Enjoying Life"
		}

	}

	// if- else if - ...else;
// 
// Logical Operators:
// 
// AND -- && this will return true if both statements are true.
// OR -- || this will return true if one of the statements is true.