//**
//This function convers a number into words @param {number} number || the number we want to convert @return {string} converted number to words.
//
//**

//step1: Create an array of words where we will get the converted value

//Goal 1: Convert 0-9;
const oneDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];

//Goal 2: Convert 10-90 (Multiples of 10)
// 10, 20, 30 , 40, 50, 60, 70, 80, 90
//Divide the data by 10 and you'll fet the index - 1;



const tens = ["","Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" ];


//Goal 3: we have to make sure that Goal#2 will only check the numbers less than 100

//Goal 4: 11-19
const teens = ["", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];

//Goal 5: 

function converter(number){
	if (number <= 9 ) {
		return oneDigit[number];
	}

	//Goal 2
	//to check if a number is a multiple of ten, the remainder when the number is divide by 10 should be zero

	//Goal 3
	//We added another condition to make sure that the number will divisible by 10 AND less than 100.
	if (number % 10 === 0 && number < 100) {
		const tensIndex = number/10
		return tens[tensIndex];
	}

	//Goal 4: 11, 12, 13, 14, 15, 16, 17, 18, 19
	if ( number > 10 && number < 20) {
		//we can get the index by subtracting by 10 from the number.
		const teensIndex = number - 10;
		return teens[teensIndex];
	}

	//Goal #5. -3/4 -- thirty four
	//the goal of this function is to separate the first digit and the last digit (onesIndex). Use them as an index to get the value from the previously created arrays
	if ( number < 100) {
		//to get the last digit, we will get the remainder of the number when divided by 10 (%10)
		const onesIndex = number % 10;
		//to get the first digit, we need to divide the number by 10. BUT make sure first that there will be no remainders.
		const numberWihtoutRemainder = number - onesIndex;
		const tensIndex = numberWihtoutRemainder/10;
		return tens[tensIndex] + " " + oneDigit[onesIndex];
	}


	if ( (number > 100 && number <= 109)||
		(number > 200 && number <= 209)||
		(number > 300 && number <= 309)||
		(number > 400 && number <= 409)||
		(number > 500 && number <= 509)||
		(number > 600 && number <= 609)||
		(number > 700 && number <= 709)||
		(number > 800 && number <= 809)||
		(number > 900 && number <= 909)){

		const ones = number % 10;
		const hundredvalue = number - ones;
		const hundred = hundredvalue / 100;
		return oneDigit[hundred] + " hundred "+ oneDigit[ones];
	}

	if ( (number > 110 && number <= 119)||
		(number > 210 && number <= 219)||
		(number > 310 && number <= 319)||
		(number > 410 && number <= 419)||
		(number > 510 && number <= 519)||
		(number > 610 && number <= 619)||
		(number > 710 && number <= 719)||
		(number > 810 && number <= 819)||
		(number > 910 && number <= 919))
	{

		const ones = number % 10;
		const hundredvalue = number % 100;
		const hundredIndex = number - hundredvalue;
		const hundred = hundredIndex / 100;
		return oneDigit[hundred] + " hundred " + teens[ones];
	}



	if (number > 100 && number % 10 === 0 && number <= 990) {
		const tenRemainder = number % 100;
		const hundredvalue = number - tenRemainder;
		const hundred = hundredvalue / 100;
		const tenValue = tenRemainder / 10;
		return oneDigit[hundred] + " hundred " + tens[tenValue];

	}

	if (number > 100 && number <= 999) {
		const ones = number % 10;
		const tenRemainder = number % 100;
		const hundredvalue = number - tenRemainder;
		const hundred = hundredvalue / 100;
		const tenValue = (tenRemainder - ones ) / 10;
		return oneDigit[hundred] + " hundred " + tens[tenValue] + " " + oneDigit[ones];

	}


	if ( (number > 1000 && number <= 1009)||
		(number > 2000 && number <= 2009)||
		(number > 3000 && number <= 3009)||
		(number > 4000 && number <= 4009)||
		(number > 5000 && number <= 5009)||
		(number > 6000 && number <= 6009)||
		(number > 7000 && number <= 7009)||
		(number > 8000 && number <= 8009)||
		(number > 9000 && number <= 9009)){

		const thousandOnes = number % 1000;
		const thousandIndex = number - thousandOnes;
		const thousand = thousandIndex / 1000;
		return oneDigit[thousand] + " thousand " + oneDigit[thousandOnes];
	}

	if ( (number > 1010 && number <= 1019)||
		(number > 2010 && number <= 2019)||
		(number > 3010 && number <= 3019)||
		(number > 4010 && number <= 4019)||
		(number > 5010 && number <= 5019)||
		(number > 6010 && number <= 6019)||
		(number > 7010 && number <= 7019)||
		(number > 8010 && number <= 8019)||
		(number > 9010 && number <= 9019))
	{
		const thounsandOnes = number  % 10;
		const thousandTen = number % 1000;
		const thousandIndex = number - thousandTen;
		const thousand = thousandIndex / 1000;
		return oneDigit[thousand] + " thousand " + teens[thounsandOnes];
	}



	if (number % 1000 === 0 && number <= 9000) 
	{
		const thousand = number / 1000;
		return oneDigit[thousand] + " thousand";
	}


	if (number % 10 === 0 && number >= 1010 && number  <= 9090) 
	{
		const tensRemainder = number % 100;
		const thousandIndex = number - tensRemainder;
		const thousand = thousandIndex / 1000;
		const tensValue = tensRemainder / 10;
		return oneDigit[thousand] + " thousand " + tens[tensValue];

	}



	return "Invalid Data";
	
	//the keyword return will terminate the function so therefore the tasks below the return statement will not be executed
}





